# Les modèles démographiques

## Modéliser l'évolution d'une population

Modéliser l'évolution d'une population, c'est d'abord représenter graphiquement les données par un nuage de points, puis analyser les données en calculant la variation absolue et le taux de variation entre les années successives.

Pour cela, nous choisissons une année de référence, dite année $0$. Chaque année suivante sera numérotée à partir de cette année. Les populations successives $u(n)$ sont alors des fonctions de la variable $n$.

---

!!! tip "*Définition* : Variation absolue"

    Définition : entre les années n et n+1, la *variation absolue* est 
    
    $\mathbf{u(n+1)-u(n)}$
    
    autrement dit 
    
    $\mathbf{V_{finale}-V_{initiale}}$


!!! tip "*Définition* : Taux de variation"

    Définition : entre les années n et n+1, le *taux de variation* est 
    
    $\mathbf{\dfrac{u(n+1)-u(n)}{u(n)}}$
    
    autrement dit 
    
    $\mathbf{\dfrac{V_{finale}-V_{initiale}}{V_{initiale}}}$


Pour finir, modéliser l'évolution, c'est trouver une loi qui lie les valeurs $u(n)$ entre elles, afin de comprendre comment la population évolue, et de prévoir l'évolution future.

??? example "Exemple : population de Paris"

    Étudions la population de Paris entre 2006 et 2016, exprimée en milliers d'habitants. L'année de référence et 2006, numérotée 0, et donc $u(0)=2181$. À l'aide d'un tableur, nous pouvons rapidement calculer les variations absolues et les taux de variation. 
    
    | Année |  n | Population (×1000) | Variation absolue (×1000) | Taux de variation |
    |-------|---:|:------------------:|:-------------------------:|:-----------------:|
    | 2006  |  0 |        2181        |                           |                   |
    | 2007  |  1 |        2193        |             12            |       0,6 %       |
    | 2008  |  2 |        2211        |             18            |       0,8 %       |
    | 2009  |  3 |        2234        |             23            |       1,0 %       |
    | 2010  |  4 |        2244        |             10            |       0,4 %       |
    | 2011  |  5 |        2250        |             6             |       0,3 %       |
    | 2012  |  6 |        2241        |             -9            |       -0,4 %      |
    | 2013  |  7 |        2230        |            -11            |       -0,5 %      |
    | 2014  |  8 |        2220        |            -10            |       -0,4 %      |
    | 2015  |  9 |        2206        |            -14            |       -0,6 %      |
    | 2016  | 10 |        2190        |            -16            |       -0,7 %      |
    
    Nous pouvons également représenter le nuage de points :
