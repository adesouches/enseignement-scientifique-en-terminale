# Enseignement scientifique en Terminale (partie maths)

[Cours sur Moodle](https://0680010s.moodle.monbureaunumerique.fr/course/view.php?id=733 "Sur le site du lycée Blaise Pascal"){ .md-button }

## Les thèmes au programme

* Les modèles démographiques
* L'intelligence artificielle
* La biodiversité et son évolution
* Optimisation du transport de l'électricité

---

!!! info "Pourquoi ?"
    Elles permettent de délimiter des blocs de contenu
     avec une touche de couleur, sans créer de nouvelles
     entrées dans la table des matières.

    Elles structurent donc sans alourdir les onglets de navigation.

---

$$\pi\approx 3,1415$$

2^e^ essai :

$f(x)=\dfrac{\mathrm{e}^x}{\sin{x}}$

---

=== "`C`"

    ```c
    #include <stdio.h>

    int main(void) {
      printf("Hello world!\n");
      return 0;
    }
    ```

=== "`C++`"

    ```cpp
    #include <iostream>

    int main(void) {
      std::cout << "Hello world!" << std::endl;
      return 0;
    }
    ```

=== "`Python`"

    ```python
    print("Hello world!")
    ```

---

Lorem ipsum[^ip] dolor sit amet, consectetur adipiscing[^ad] elit.[^el]

[^ip]: Lorem ipsum dolor sit amet, consectetur adipiscing elit.

[^ad]:
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

[^el]:
    Ce texte [Lorem ipsum ...](https://fr.wikipedia.org/wiki/Lorem_ipsum)
    est un faux-texte destiné à remplir la vide.

Une liste de tâches

- [ ] à faire
- [x] fini
- [ ] presque
- [x] fait depuis longtemps

JEU_52 = (
    '🂡🂢🂣🂤🂥🂦🂧🂨🂩🂪🂫🂭🂮',
    '🂱🂲🂳🂴🂵🂶🂷🂸🂹🂺🂻🂽🂾',
    '🃁🃂🃃🃄🃅🃆🃇🃈🃉🃊🃋🃍🃎',
    '🃑🃒🃓🃔🃕🃖🃗🃘🃙🃚🃛🃝🃞',
    '🂠🂬🂼🃌🃜🃏🃟'
    )


---

> **Cookie** :
>
> - Anciennement petit gâteau sucré, qu'on acceptait avec plaisir.
> - Aujourd'hui : petit fichier informatique drôlement salé, qu'il faut refuser avec véhémence.

De Luc Fayard, _Dictionnaire impertinent des branchés_

---

La définition de la fonction `premier` commence avec le mot clé `def`

Elle prend en paramètre un entier `n`

Elle renvoie un booléen avec le mot clé `return`

---

| Objectif | Markdown | Rendu |
|:---------|-------------:|:---:|
| Créer un lien    | `[texte cliquable](https://mon_lien.fr)` | [texte cliquable](https://mon_lien.fr) |
| Emphase faible   | `Un _mot_ discret`    | Un _mot_ discret   |
| Emphase forte    | `Un **mot** visible`  | Un **mot** visible |
| Du code en ligne | ``Une boucle `for` `` | Une boucle `for`   |

